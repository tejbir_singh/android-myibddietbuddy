package com.example.myibddietbuddy;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
//import android.support.v7.internal.widget.AdapterViewICS.OnItemClickListener;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Build;

public class MainActivity extends Activity implements OnItemClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		ListView lv = (ListView) findViewById(R.id.listview1);
		lv.setClickable(true);
		lv.setOnItemClickListener((OnItemClickListener) this);
	}
	
	public void onItemClick(AdapterView<?> l, View view, int position, long id)
	{
		Log.i("HelloListView", "You clicked item:" + id + " at position:" + position);
		// now start new activity via intent
		String condition = ((TextView) view).getText().toString();
		Intent intent = new Intent();
		intent.setClass(this, ConditionItemDetail.class);
		intent.putExtra("position", position);
		intent.putExtra("id", id);
		intent.putExtra("condition", condition);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
